#region License and Terms

/*
The MIT License (MIT)

Copyright (c) 2013-2014 Vitalij <vitalij@gmx.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#endregion License and Terms

#region Using declarations

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Data;
using NinjaTrader.MarketAnalyzer;
using Vitalij.Drawing;
using Vitalij.NinjaTrader7.Alerts;
using Vitalij.Windows.Forms.Alignment;
using NtDisplayName = NinjaTrader.Gui.Design.DisplayNameAttribute;

#endregion Using declarations

namespace NinjaTrader.Indicator {

	[Description("Vitalij's Level II indicator for NinjaTrader 7" + "\r\n" + "http://ninjatrader.bitbucket.org/")]
	public class VitLevel2 : Indicator {

		#region Variables

		public enum Bool { Yes, No }

		public enum ShowVolume { Each, Biggest, Nearest, No }

		public enum CalculateSum { ByVisibleDepth, ByEntireDepth, No }

		public enum AlertMode { Never, OnDisplayUpdate, OnMarketDepth }

		private bool showBars = true;
		private ShowVolume showVolume = ShowVolume.Biggest;
		private CalculateSum calculateSum = CalculateSum.ByVisibleDepth;

		private const string errorMsg = "No market depth data available!";

		private byte opacityBarAsk = 50;
		private byte opacityBarBid = 50;

		private int maxBarWidth = 100;
		private int maxBarHeight = 50;
		private int minBarHeight = 3;
		private int barSpacing = 1;

		private int marginLeft = 0;
		private int marginRight = 0;

		private int depth = 0;

		private Dictionary<double, long> rowsAsk = new Dictionary<double, long>();
		private Dictionary<double, long> rowsBid = new Dictionary<double, long>();
		private IEnumerable<KeyValuePair<double, long>> rowsA;
		private IEnumerable<KeyValuePair<double, long>> rowsB;

		private Pen penOrderBookMedian = new Pen(Color.Silver) { DashStyle = System.Drawing.Drawing2D.DashStyle.Dot };

		private Font font;
		private Font scaledFont;
		private bool scaleFont = false;

		private HAlignment aliIndicator = HAlignment.Right;
		private HAlignment aliBar = HAlignment.Right;
		private HAlignment aliVolumeAsk = HAlignment.Left;
		private HAlignment aliVolumeBid = HAlignment.Left;
		private HAlignment aliSumAsk = HAlignment.Right;
		private HAlignment aliSumBid = HAlignment.Right;

		private StringFormat sfErrorMsg;
		private StringFormat sfVolumeAsk;
		private StringFormat sfVolumeBid;
		private StringFormat sfSumAsk;
		private StringFormat sfSumBid;

		private Color colorBarAsk = Color.Red;
		private Color colorBarBid = Color.LimeGreen;
		private Color colorVolumeAsk = Color.Black;
		private Color colorVolumeBid = Color.Black;
		private Color colorSumAsk = Color.Black;
		private Color colorSumBid = Color.Black;

		private Brush brushErrorMsg;
		private Brush brushBarAsk;
		private Brush brushBarBid;
		private Brush brushVolumeAsk;
		private Brush brushVolumeBid;
		private Brush brushSumAsk;
		private Brush brushSumBid;

		private AlertConditionCollection alertOnMaxSize = new AlertConditionCollection();
		private AlertConditionCollection alertOnSpread = new AlertConditionCollection();
		private AlertConditionCollection alertOnMaxSizePercentage = new AlertConditionCollection();
		private AlertConditionCollection alertOnSumPercentage = new AlertConditionCollection();

		#endregion Variables

		#region Functions

		protected override void Initialize() {
			CalculateOnBarClose = true;
			Overlay = true;
			AutoScale = false;

			Name = "Vitalij's Level II Indicator";

			// dirty hack to load default settings
			if (ChartControl == null && System.Windows.Forms.Form.ActiveForm != null && System.Windows.Forms.Form.ActiveForm.Modal) {
				ChartControl = (NinjaTrader.Gui.Chart.ChartControl)System.Windows.Forms.Form.ActiveForm.GetType().GetField("chartControl", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(System.Windows.Forms.Form.ActiveForm);
				colorBarAsk = ChartControl.DownColor;
				colorBarBid = ChartControl.UpColor;
				colorVolumeAsk = colorVolumeBid = colorSumAsk = colorSumBid = ChartControl.AxisColor;
				font = (Font)ChartControl.Font.Clone();
			}
		}

		protected override void OnStartUp() {
			scaledFont = (Font)font.Clone();

			brushErrorMsg = new SolidBrush(ChartControl.AxisColor);
			brushBarAsk = new SolidBrush(Color.FromArgb(opacityBarAsk * 255 / 100, colorBarAsk));
			brushBarBid = new SolidBrush(Color.FromArgb(opacityBarBid * 255 / 100, colorBarBid));
			brushVolumeAsk = new SolidBrush(colorVolumeAsk);
			brushVolumeBid = new SolidBrush(colorVolumeBid);
			brushSumAsk = new SolidBrush(colorSumAsk);
			brushSumBid = new SolidBrush(colorSumBid);

			sfErrorMsg = new StringFormat { Alignment = StringAlignment.Far, LineAlignment = StringAlignment.Far };
			sfVolumeAsk = aliVolumeAsk.ToStringFormat(VAlignment.Middle);
			sfVolumeBid = aliVolumeBid.ToStringFormat(VAlignment.Middle);
			sfSumAsk = aliSumAsk.ToStringFormat(VAlignment.Top);
			sfSumBid = aliSumBid.ToStringFormat(VAlignment.Bottom);

			barWidthScale = 1 / ContractsPerPixel;
		}

		protected override void OnTermination() {
			sfErrorMsg.Dispose();
			sfVolumeAsk.Dispose();
			sfVolumeBid.Dispose();
			sfSumAsk.Dispose();
			sfSumBid.Dispose();

			brushErrorMsg.Dispose();
			brushBarAsk.Dispose();
			brushBarBid.Dispose();
			brushVolumeAsk.Dispose();
			brushVolumeBid.Dispose();
			brushSumAsk.Dispose();
			brushSumBid.Dispose();

			scaledFont.Dispose();

			//font.Dispose(); // FIXME: reinitialize in OnStartUp() or Plot() will fail!
			//penOrderBookMedian.Dispose(); // FIXME: reinitialize in OnStartUp() or Plot() will fail!
		}

		protected override void OnBarUpdate() {
		}

		protected override void OnMarketDepth(MarketDepthEventArgs ev) {
			if (ev.MarketDataType == MarketDataType.Ask) {
				SetMarketDepthVolume(ev, rowsAsk);
			} else if (ev.MarketDataType == MarketDataType.Bid) {
				SetMarketDepthVolume(ev, rowsBid);
			}
		}

		protected void SetMarketDepthVolume(MarketDepthEventArgs ev, Dictionary<double, long> rows) {
			if (ev.Price == 0) {
				rowsAsk.Clear();
				rowsBid.Clear();
			}

			if (ev.Operation == Operation.Remove) {
				rows.Remove(ev.Price);
			} else /* if (ev.Operation == Operation.Insert || ev.Operation == Operation.Update) */ {
				rows[ev.Price] = ev.Volume;
			}

			ChartControl.ChartPanel.Invalidate();

			if (rowsAsk.Count == 0 || rowsBid.Count == 0) {
				return;
			}

			if (Alert == AlertMode.OnMarketDepth) {
				CheckAlerts();
			}
		}

		private Graphics graphics; // Plot() specific vars
		private float barWidthScale; // Plot() specific vars
		private int barOffsetY; // Plot() specific vars
		private int left; // Plot() specific vars

		public override void Plot(Graphics graphics, Rectangle bounds, double min, double max) {
			this.graphics = graphics;

			// display error msg
			if (ChartControl == null || Bars == null || Bars.Count < 1 || Bars.MarketData == null || Bars.MarketData.Connection.PriceStatus != Cbi.ConnectionStatus.Connected || rowsAsk.Count == 0 || rowsBid.Count == 0) {
				graphics.DrawString(errorMsg, ChartControl.Font, brushErrorMsg, bounds.Right, bounds.Bottom, sfErrorMsg);
				return;
			}

			// limit order book depth
			if (depth > 0) {
				rowsA = rowsAsk.OrderBy(kv => kv.Key).Take(depth);
				rowsB = rowsBid.OrderByDescending(kv => kv.Key).Take(depth);
			} else {
				rowsA = rowsAsk;
				rowsB = rowsBid;
			}

			var maxAskVolume = rowsA.Max(v => v.Value);
			var maxBidVolume = rowsB.Max(v => v.Value);
			var minKeyAsk = rowsA.Min(v => v.Key);
			var maxKeyBid = rowsB.Max(v => v.Key);

			// calc bar scale (ContractsPerPixel is default value)
			if (ContractsPerPixel == 0) {
				barWidthScale = maxBarWidth / (float)Math.Max(maxAskVolume, maxBidVolume);
			}

			// calc indicator x-position
			var l = bounds.Left + marginLeft;
			var r = bounds.Right - marginRight - maxBarWidth;
			switch (aliIndicator) {
				case HAlignment.Left:
					left = l;
					break;

				case HAlignment.Center:
					left = (l + r) / 2;
					break;

				default:
				case HAlignment.Right:
					left = r;
					break;
			}

			// calc bar height and y-offset
			var height = LimitHeight(ChartControl.GetYByValue(ChartControl.Bars[0], 0) - ChartControl.GetYByValue(ChartControl.Bars[0], TickSize));
			barOffsetY = height / 2;

			// cramp font to fit bar height
			if (scaleFont && scaledFont.Size != height) {
				scaledFont.Dispose();
				scaledFont = new Font(font.FontFamily, Math.Min(font.SizeInPixel(graphics), height), font.Style, GraphicsUnit.Pixel, font.GdiCharSet, font.GdiVerticalFont);
			}

			// draw bars
			foreach (KeyValuePair<double, long> row in rowsA) {
				if (showBars) {
					DrawBar(brushBarAsk, row);
				}
				if (showVolume == ShowVolume.Each || (showVolume == ShowVolume.Biggest && row.Value == maxAskVolume) || (showVolume == ShowVolume.Nearest && row.Key == minKeyAsk)) {
					DrawText(row.Key, row.Value, scaledFont, brushVolumeAsk, aliVolumeAsk, sfVolumeAsk);
				}
			}

			foreach (KeyValuePair<double, long> row in rowsB) {
				if (showBars) {
					DrawBar(brushBarBid, row);
				}
				if (showVolume == ShowVolume.Each || (showVolume == ShowVolume.Biggest && row.Value == maxBidVolume) || (showVolume == ShowVolume.Nearest && row.Key == maxKeyBid)) {
					DrawText(row.Key, row.Value, scaledFont, brushVolumeBid, aliVolumeBid, sfVolumeBid);
				}
			}

			// draw sums
			if (calculateSum != CalculateSum.No) {
				DrawText(rowsA.Max(kv => kv.Key) + TickSize, (calculateSum == CalculateSum.ByEntireDepth ? rowsAsk : rowsA).Sum(v => v.Value), font, brushSumAsk, aliSumAsk, sfSumAsk);
				DrawText(rowsB.Min(kv => kv.Key) - TickSize, (calculateSum == CalculateSum.ByEntireDepth ? rowsBid : rowsB).Sum(v => v.Value), font, brushSumBid, aliSumBid, sfSumBid);
			}

			// draw median
			if (penOrderBookMedian.Color != Color.Transparent) {
				var median = (ChartControl.GetYByValue(ChartControl.Bars[0], minKeyAsk) + ChartControl.GetYByValue(ChartControl.Bars[0], maxKeyBid)) / 2;
				graphics.DrawLine(penOrderBookMedian, bounds.Left, median, bounds.Right, median);
			}

			if (Alert == AlertMode.OnDisplayUpdate) {
				CheckAlerts();
			}
		}

		private void DrawBar(Brush brush, KeyValuePair<double, long> row) {
			var top = ChartControl.GetYByValue(ChartControl.Bars[0], row.Key);
			var bottom = ChartControl.GetYByValue(ChartControl.Bars[0], row.Key - TickSize);

			var width = (int)Math.Round(row.Value * barWidthScale);
			var height = LimitHeight(bottom - top);

			var x = Left(aliBar, width);
			var y = top - barOffsetY;

			graphics.FillRectangle(brush, x, y, width, height);
		}

		private void DrawText(double price, object text, Font font, Brush brush, HAlignment textAlignment, StringFormat stringFormat) {
			var x = Left(textAlignment, 0);
			var y = ChartControl.GetYByValue(ChartControl.Bars[0], price);

			graphics.DrawString(text.ToString(), font, brush, x, y, stringFormat);
		}

		private int Left(HAlignment hAlignment, int barWidth) {
			switch (hAlignment) {
				case HAlignment.Left:
					return left;

				case HAlignment.Center:
					return left + maxBarWidth / 2 - (barWidth / 2);

				case HAlignment.Right:
					return left + maxBarWidth - barWidth;
			}

			throw new Exception("Unknown horizontal alignment value: " + hAlignment);
		}

		private int LimitHeight(int height) {
			height -= barSpacing;

			if (height > maxBarHeight) {
				height = maxBarHeight;
			}
			if (height < minBarHeight) {
				height = minBarHeight;
			}

			return height;
		}

		private void CheckAlerts() {
			// TODO: do not block; run on other thread

			if (alertOnMaxSize.Count > 0) {
				this.Fire(alertOnMaxSize, Math.Max(rowsAsk.Max(kv => kv.Value), rowsBid.Max(kv => kv.Value)));
			}

			if (alertOnSpread.Count > 0) {
				this.Fire(alertOnSpread, Math.Round((rowsAsk.OrderByDescending(kv => kv.Key).Last().Key - rowsBid.OrderByDescending(kv => kv.Key).First().Key) / TickSize, 0));
			}

			if (alertOnMaxSizePercentage.Count > 0) {
				this.Fire(alertOnMaxSizePercentage, 100 * rowsAsk.Max(kv => kv.Value) / rowsBid.Max(kv => kv.Value));
			}

			if (alertOnSumPercentage.Count > 0) {
				this.Fire(alertOnSumPercentage, 100 * rowsAsk.Sum(kv => kv.Value) / rowsBid.Sum(kv => kv.Value));
			}
		}

		#endregion Functions

		#region Properties

		[XmlIgnore()]
		[Category("02. Data")]
		[NtDisplayName("01. Show bars")]
		public Bool ShowBars {
			get { return showBars ? Bool.Yes : Bool.No; }
			set { showBars = value == Bool.Yes; }
		}

		[Browsable(false)]
		public bool ShowBarsSerialize {
			get { return showBars; }
			set { showBars = value; }
		}

		[XmlIgnore()]
		[Category("02. Data")]
		[NtDisplayName("02. Show volume")]
		[Description("Each = Show volume on each row\r\nBiggest = Show only biggest volume\r\nNearest = Show only ask/bid volume\r\nNo = Hide volume")]
		public ShowVolume ShowVolumeProperty {
			get { return showVolume; }
			set { showVolume = value; }
		}

		[XmlIgnore()]
		[Category("02. Data")]
		[NtDisplayName("03. Scale volume font")]
		public Bool ScaleFont {
			get { return scaleFont ? Bool.Yes : Bool.No; }
			set { scaleFont = value == Bool.Yes; }
		}

		[Browsable(false)]
		public bool ScaleFontSerialize {
			get { return scaleFont; }
			set { scaleFont = value; }
		}

		[Browsable(false)]
		public string ShowVolumePropertySerialize {
			get { return TypeDescriptor.GetConverter(typeof(ShowVolume)).ConvertToInvariantString(showVolume); }
			set { showVolume = (ShowVolume)TypeDescriptor.GetConverter(typeof(ShowVolume)).ConvertFromInvariantString(value); }
		}

		[Category("02. Data")]
		[NtDisplayName("04. Visible depth")]
		[Description("0 = no restrictions")]
		public int Depth {
			get { return depth; }
			set { depth = Math.Max(value, 0); }
		}

		[XmlIgnore()]
		[Category("02. Data")]
		[NtDisplayName("05. Calculate sums")]
		public CalculateSum CalculateSumProperty {
			get { return calculateSum; }
			set { calculateSum = value; }
		}

		[Browsable(false)]
		public string CalculateSumPropertySerialize {
			get { return TypeDescriptor.GetConverter(typeof(CalculateSum)).ConvertToInvariantString(calculateSum); }
			set { calculateSum = (CalculateSum)TypeDescriptor.GetConverter(typeof(CalculateSum)).ConvertFromInvariantString(value); }
		}

		[Category("03. Settings")]
		[NtDisplayName("01. Contracts per pixel")]
		[Description("Absolute bar width.\r\nOverrides relative max bar width.\r\nDefault = 0 (do not override)")]
		public float ContractsPerPixel { get; set; }

		[Category("03. Settings")]
		[NtDisplayName("02. Bar width (max.)")]
		public int MaxBarWidthProperty {
			get { return maxBarWidth; }
			set { maxBarWidth = Math.Max(value, 1); }
		}

		[Category("03. Settings")]
		[NtDisplayName("03. Bar height (min.)")]
		public int MinBarHeightProperty {
			get { return minBarHeight; }
			set { minBarHeight = Math.Max(Math.Min(value, maxBarHeight), 1); }
		}

		[Category("03. Settings")]
		[NtDisplayName("04. Bar height (max.)")]
		public int MaxBarHeightProperty {
			get { return maxBarHeight; }
			set { maxBarHeight = Math.Max(value, minBarHeight); }
		}

		[Category("03. Settings")]
		[NtDisplayName("05. Bar spacing")]
		public int BarSpacing {
			get { return barSpacing; }
			set { barSpacing = Math.Max(value, 0); }
		}

		[Category("03. Settings")]
		[NtDisplayName("06. Margin left")]
		public int MarginLeftProperty {
			get { return marginLeft; }
			set { marginLeft = Math.Max(value, 0); }
		}

		[Category("03. Settings")]
		[NtDisplayName("07. Margin right")]
		public int MarginRightProperty {
			get { return marginRight; }
			set { marginRight = Math.Max(value, 0); }
		}

		[XmlIgnore()]
		[Category("04. Alignment")]
		[NtDisplayName("01. Indicator")]
		public HAlignment AliIndicatorProperty {
			get { return aliIndicator; }
			set { aliIndicator = value; }
		}

		[Browsable(false)]
		public string AliIndicatorPropertySerialize {
			get { return TypeDescriptor.GetConverter(typeof(HAlignment)).ConvertToInvariantString(aliIndicator); }
			set { aliIndicator = (HAlignment)TypeDescriptor.GetConverter(typeof(HAlignment)).ConvertFromInvariantString(value); }
		}

		[XmlIgnore()]
		[Category("04. Alignment")]
		[NtDisplayName("02. Bars")]
		public HAlignment AliBarProperty {
			get { return aliBar; }
			set { aliBar = value; }
		}

		[Browsable(false)]
		public string AliBarPropertySerialize {
			get { return TypeDescriptor.GetConverter(typeof(HAlignment)).ConvertToInvariantString(aliBar); }
			set { aliBar = (HAlignment)TypeDescriptor.GetConverter(typeof(HAlignment)).ConvertFromInvariantString(value); }
		}

		[XmlIgnore()]
		[Category("04. Alignment")]
		[NtDisplayName("03. Sum ask")]
		public HAlignment AliSumAsk {
			get { return aliSumAsk; }
			set { aliSumAsk = value; }
		}

		[Browsable(false)]
		public string AliSumAskSerialize {
			get { return TypeDescriptor.GetConverter(typeof(HAlignment)).ConvertToInvariantString(aliSumAsk); }
			set { aliSumAsk = (HAlignment)TypeDescriptor.GetConverter(typeof(HAlignment)).ConvertFromInvariantString(value); }
		}

		[XmlIgnore()]
		[Category("04. Alignment")]
		[NtDisplayName("04. Volume ask")]
		public HAlignment AliVolumeAsk {
			get { return aliVolumeAsk; }
			set { aliVolumeAsk = value; }
		}

		[Browsable(false)]
		public string AliVolumeAskSerialize {
			get { return TypeDescriptor.GetConverter(typeof(HAlignment)).ConvertToInvariantString(aliVolumeAsk); }
			set { aliVolumeAsk = (HAlignment)TypeDescriptor.GetConverter(typeof(HAlignment)).ConvertFromInvariantString(value); }
		}

		[XmlIgnore()]
		[Category("04. Alignment")]
		[NtDisplayName("05. Volume bid")]
		public HAlignment AliVolumeBid {
			get { return aliVolumeBid; }
			set { aliVolumeBid = value; }
		}

		[Browsable(false)]
		public string AliVolumeBidSerialize {
			get { return TypeDescriptor.GetConverter(typeof(HAlignment)).ConvertToInvariantString(aliVolumeBid); }
			set { aliVolumeBid = (HAlignment)TypeDescriptor.GetConverter(typeof(HAlignment)).ConvertFromInvariantString(value); }
		}

		[XmlIgnore()]
		[Category("04. Alignment")]
		[NtDisplayName("06. Sum bid")]
		public HAlignment AliSumBid {
			get { return aliSumBid; }
			set { aliSumBid = value; }
		}

		[Browsable(false)]
		public string AliSumBidSerialize {
			get { return TypeDescriptor.GetConverter(typeof(HAlignment)).ConvertToInvariantString(aliSumBid); }
			set { aliSumBid = (HAlignment)TypeDescriptor.GetConverter(typeof(HAlignment)).ConvertFromInvariantString(value); }
		}

		[XmlIgnore()]
		[Category("05. Colors")]
		[NtDisplayName("04. Ask bar")]
		public Color ColorBarAskProperty {
			get { return colorBarAsk; }
			set { colorBarAsk = value; }
		}

		[Browsable(false)]
		public string ColorBarAskPropertySerialize {
			get { return TypeDescriptor.GetConverter(typeof(Color)).ConvertToInvariantString(colorBarAsk); }
			set { colorBarAsk = (Color)TypeDescriptor.GetConverter(typeof(Color)).ConvertFromInvariantString(value); }
		}

		[XmlIgnore()]
		[Category("05. Colors")]
		[NtDisplayName("02. Ask volume")]
		public Color colorVolumeAskProperty {
			get { return colorVolumeAsk; }
			set { colorVolumeAsk = value; }
		}

		[Browsable(false)]
		public string colorVolumeAskPropertySerialize {
			get { return TypeDescriptor.GetConverter(typeof(Color)).ConvertToInvariantString(colorVolumeAsk); }
			set { colorVolumeAsk = (Color)TypeDescriptor.GetConverter(typeof(Color)).ConvertFromInvariantString(value); }
		}

		[XmlIgnore()]
		[Category("05. Colors")]
		[NtDisplayName("01. Ask sum")]
		public Color colorSumAskProperty {
			get { return colorSumAsk; }
			set { colorSumAsk = value; }
		}

		[Browsable(false)]
		public string colorSumAskPropertySerialize {
			get { return TypeDescriptor.GetConverter(typeof(Color)).ConvertToInvariantString(colorSumAsk); }
			set { colorSumAsk = (Color)TypeDescriptor.GetConverter(typeof(Color)).ConvertFromInvariantString(value); }
		}

		[Category("05. Colors")]
		[NtDisplayName("03. Ask bar opacity (%)")]
		public byte OpacityBarAskProperty {
			get { return opacityBarAsk; }
			set { opacityBarAsk = Math.Min(value, (byte)100); }
		}

		[XmlIgnore()]
		[Category("05. Colors")]
		[NtDisplayName("05. Bid bar")]
		public Color ColorBarBidProperty {
			get { return colorBarBid; }
			set { colorBarBid = value; }
		}

		[Browsable(false)]
		public string ColorBarBidPropertySerialize {
			get { return TypeDescriptor.GetConverter(typeof(Color)).ConvertToInvariantString(colorBarBid); }
			set { colorBarBid = (Color)TypeDescriptor.GetConverter(typeof(Color)).ConvertFromInvariantString(value); }
		}

		[XmlIgnore()]
		[Category("05. Colors")]
		[NtDisplayName("07. Bid volume")]
		public Color colorVolumeBidProperty {
			get { return colorVolumeBid; }
			set { colorVolumeBid = value; }
		}

		[Browsable(false)]
		public string colorVolumeBidPropertySerialize {
			get { return TypeDescriptor.GetConverter(typeof(Color)).ConvertToInvariantString(colorVolumeBid); }
			set { colorVolumeBid = (Color)TypeDescriptor.GetConverter(typeof(Color)).ConvertFromInvariantString(value); }
		}

		[XmlIgnore()]
		[Category("05. Colors")]
		[NtDisplayName("08. Bid sum")]
		public Color colorSumBidProperty {
			get { return colorSumBid; }
			set { colorSumBid = value; }
		}

		[Browsable(false)]
		public string colorSumBidPropertySerialize {
			get { return TypeDescriptor.GetConverter(typeof(Color)).ConvertToInvariantString(colorSumBid); }
			set { colorSumBid = (Color)TypeDescriptor.GetConverter(typeof(Color)).ConvertFromInvariantString(value); }
		}

		[Category("05. Colors")]
		[NtDisplayName("06. Bid bar opacity (%)")]
		public byte OpacityBarBidProperty {
			get { return opacityBarBid; }
			set { opacityBarBid = Math.Min(value, (byte)100); }
		}

		[XmlIgnore()]
		[Category("01. Global")]
		[NtDisplayName("01. Font")]
		public Font Font {
			get { return font; }
			set { font = value; }
		}

		[Browsable(false)]
		public string FontSerialize {
			get { return TypeDescriptor.GetConverter(typeof(Font)).ConvertToInvariantString(font); }
			set { font = (Font)TypeDescriptor.GetConverter(typeof(Font)).ConvertFromInvariantString(value); }
		}

		[XmlIgnore()]
		[Category("01. Global")]
		[NtDisplayName("02. Median line")]
		[TypeConverter(typeof(NinjaTrader.Gui.Design.PenConverter))]
		[Editor(typeof(NinjaTrader.Gui.Design.PenEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public Pen PenOrderBookMedianProperty {
			get { return penOrderBookMedian; }
			set { penOrderBookMedian = (value == null) ? new Pen(Color.Silver) { DashStyle = System.Drawing.Drawing2D.DashStyle.Dot } : value; }
		}

		[Browsable(false)]
		public NinjaTrader.Gui.Design.SerializablePen PenOrderBookMedianPropertySerialize {
			get { return NinjaTrader.Gui.Design.SerializablePen.FromPen(penOrderBookMedian); }
			set { penOrderBookMedian = NinjaTrader.Gui.Design.SerializablePen.ToPen(value); }
		}

		[XmlIgnore()]
		[Category("06. Alerts")]
		[NtDisplayName("01. Alert Mode")]
		[Description("OnMarketDepth: very resource-intensive\r\nOnDisplayUpdate: challenging\r\nNever: free")]
		public AlertMode Alert { get; set; }

		[Browsable(false)]
		public string AlertSerialize {
			get { return TypeDescriptor.GetConverter(typeof(AlertMode)).ConvertToInvariantString(Alert); }
			set { Alert = (AlertMode)TypeDescriptor.GetConverter(typeof(AlertMode)).ConvertFromInvariantString(value); }
		}

		[Category("06. Alerts")]
		[NtDisplayName("02. Max size")]
		[Description("value = size of biggest order in orderbook\r\ne.g. alert if order size > 1000")]
		public AlertConditionCollection AlertOnMaxSize {
			get { return alertOnMaxSize; }
			set { alertOnMaxSize = value; }
		}

		[Category("06. Alerts")]
		[NtDisplayName("03. Spread")]
		[Description("value = ask - bid\r\nalert if spread > 3")]
		public AlertConditionCollection Spread {
			get { return alertOnSpread; }
			set { alertOnSpread = value; }
		}

		[Category("06. Alerts")]
		[NtDisplayName("04. Max percentage")]
		[Description("value = biggest ask order / biggest bid order * 100\r\nalert if biggest ask order is more then 200% of biggest bid order")]
		public AlertConditionCollection AlertOnMaxPercentage {
			get { return alertOnMaxSizePercentage; }
			set { alertOnMaxSizePercentage = value; }
		}

		[Category("06. Alerts")]
		[NtDisplayName("04. Sum percentage")]
		[Description("value = sum of all ask orders / sum of all bid orders * 100\r\nalert if all ask orders is more then 200% of biggest bid order")]
		public AlertConditionCollection AlertOnSumPercentage {
			get { return alertOnSumPercentage; }
			set { alertOnSumPercentage = value; }
		}

		#endregion Properties
	}
}

#region Helper & Extensions

namespace Vitalij.NinjaTrader7.Alerts {

	static public class AlertExtensions {
		static private Dictionary<AlertCondition, double> previousValues = new Dictionary<AlertCondition, double>();

		static public bool Met(this AlertCondition condition, double value) {
			switch (condition.Condition) {
				case Condition.CrossAbove:
				case Condition.CrossBelow:
					double previous;
					if (previousValues.TryGetValue(condition, out previous)) {
						if (condition.Condition == Condition.CrossAbove && previous <= condition.Value && value > condition.Value) {
							return true;
						} else if (condition.Condition == Condition.CrossBelow && previous >= condition.Value && value < condition.Value) {
							return true;
						}
					}
					previousValues[condition] = value;
					break;

				case Condition.Equals:
					return value == condition.Value;

				case Condition.Greater:
					return value > condition.Value;

				case Condition.GreaterEqual:
					return value >= condition.Value;

				case Condition.Less:
					return value < condition.Value;

				case Condition.LessEqual:
					return value <= condition.Value;

				case Condition.NotEqual:
					return value != condition.Value;
			}

			return false;
		}

		static public void Fire(this NinjaTrader.Indicator.IndicatorBase indi, AlertConditionCollection conditions, double value) {
			foreach (AlertCondition condition in conditions) {
				if (condition.Met(value)) {
					indi.Alert(condition.Id, condition.Priority, condition.Message + "\t[" + value + "]", condition.SoundLocation, condition.RearmSeconds, condition.BackColor, condition.ForeColor);
				}
			}
		}
	}
}

namespace Vitalij.Windows.Forms.Alignment {

	public enum HAlignment { Left, Center, Right } // same as System.Windows.Forms.HorizontalAlignment

	public enum VAlignment { Top, Middle, Bottom } // missing in System.Windows.Forms namespace

	static public class Extensions {

		static private StringAlignment ToStringAlignment(this HAlignment hAlignment) {
			switch (hAlignment) {
				case HAlignment.Left:
					return StringAlignment.Near;

				case HAlignment.Center:
					return StringAlignment.Center;

				case HAlignment.Right:
					return StringAlignment.Far;
			}

			throw new Exception("Unknown horizontal alignment value: " + hAlignment);
		}

		static private StringAlignment ToStringAlignment(this VAlignment vAlignment) {
			switch (vAlignment) {
				case VAlignment.Top:
					return StringAlignment.Far;

				case VAlignment.Middle:
					return StringAlignment.Center;

				case VAlignment.Bottom:
					return StringAlignment.Near;
			}

			throw new Exception("Unknown vertical alignment value: " + vAlignment);
		}

		static public StringFormat ToStringFormat(this HAlignment hAlignment, VAlignment vAlignment) {
			return new StringFormat {
				Alignment = hAlignment.ToStringAlignment(),
				LineAlignment = vAlignment.ToStringAlignment(),
			};
		}

		static public StringFormat ToStringFormat(this VAlignment vAlignment, HAlignment hAlignment) {
			return new StringFormat {
				Alignment = hAlignment.ToStringAlignment(),
				LineAlignment = vAlignment.ToStringAlignment(),
			};
		}
	}
}

namespace Vitalij.Drawing {

	static public class FontExtensions {

		static public float SizeInPixel(this Font font, Graphics graphics) {
			return font.SizeInPoints * graphics.DpiX / 72;
		}
	}
}

#endregion Helper & Extensions
