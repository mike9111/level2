# Vitalij's Level II indicator for NinjaTrader 7
##### A free, minimalistic and reliable Level 2 indicator.

### Download and Installation
#### [Zip-Archive for NinjaTrader 7 import](//bitbucket.org/ninjatrader/level2/downloads/VitLevel2.zip)
#### [Install help and HowTo](//bitbucket.org/ninjatrader/level2/wiki)
#### [Changelog](//bitbucket.org/ninjatrader/level2/src/master/CHANGELOG.md)
#### [Commit history](//bitbucket.org/ninjatrader/level2/commits)

### to create ZIP-Archive for NT7 import, run:

```sh
# zip -r -o -9 VitLevel2-latest.zip *
```

## Download and extract
Download the [Zip-Archive](https://bitbucket.org/ninjatrader/level2/get/master.zip) and extract all ``*.cs`` files in to ``Documents\NinjaTrader 7\bin\Custom\Indicator`` directory.


## Compilation and Installation

##### Step 1
Open indicator repertory.

![Step 1](https://bytebucket.org/ninjatrader/level2/wiki/img/lvl2_compile_step1.png)


##### Step 2
Doubleclick the indicator to open it in a new window.

![Step 2](https://bytebucket.org/ninjatrader/level2/wiki/img/lvl2_compile_step2.png)


##### Step 3
Click F5 to compile the indicator. You should hear a bell on success.
![Step 3](https://bytebucket.org/ninjatrader/level2/wiki/img/lvl2_compile_step3.png)


Congratulation! You can use your new level II indicator!!

![Level 2 Indicator](https://bytebucket.org/ninjatrader/level2/wiki/img/lvl2_indi.png)

## License

[The MIT License (MIT)](http://opensource.org/licenses/mit-license.php)

Copyright (c) 2013-2014 Vitalij <vitalij@gmx.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
